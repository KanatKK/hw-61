import React, {useEffect, useState} from 'react';
import axios from "axios";
import './App.css'
import Countries from "../../components/countries/countries";
import Info from "../../components/info/info";

const App = () => {
    const allUrl = 'https://restcountries.eu/rest/v2/all';
    const nameUrl = 'https://restcountries.eu/rest/v2/name/';

    const [countries, setCountries] = useState([]);
    const [selectedCountry, setSelectedCountry] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            const countriesResponse = await axios.get(allUrl);
            const promises = countriesResponse.data.map(async country => {
                const countryUrl = nameUrl + country.name;
                const currentCountryResponse = await axios.get(countryUrl);
                return {
                    ...country,
                    name: currentCountryResponse.data[0].name
                };
            });
            const newCountries = await Promise.all(promises);
            setCountries(newCountries);
        };
        fetchData().catch(e => console.log(e));
    },[]);

    return (
        <div className="container">
            <div className="scroll">
                {countries.map(country => (
                    <Countries
                        key={country.alpha3Code}
                        name={country.name}
                        clicked={() => {setSelectedCountry(country.name)}}
                    />
                ))}
            </div>
            <div className="info">
                <Info name={selectedCountry} />
            </div>
        </div>
    );
};

export default App;