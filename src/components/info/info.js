import React, {useEffect, useState} from 'react';
import './info.css'
import axios from 'axios';

const Info = props => {
    const [country, setCountry] = useState([]);
    const [bordersName, setBordersName] = useState([]);
    const [bordersCopy] = useState([]);
    const [names, setNames] = useState([])
    let borders = [...bordersCopy];
    let bordersNameCopy = [...bordersName];
    let namesCopy = [...names]

    useEffect(() => {
        const fetchData = async () => {
            if (props.name !== null) {
                const countryResponse = await axios.get('https://restcountries.eu/rest/v2/name/' + props.name);
                await setCountry(countryResponse.data);
            };
        };
        fetchData().catch(e => console.log(e));
    }, [props.name]);

    useEffect(() => {
        if (country[0] !== undefined) {
            let getBorders = () => {
                if (country[0].borders.length !== 0) {
                    country[0].borders.map(border => {
                        borders.push({border: border});
                    })
                } else {
                    namesCopy.length = 0;
                    namesCopy.push({borderName: 'Does not have any borders!'});
                    setNames(namesCopy);
                }
            };
            getBorders();
        };
    }, [country]);

    useEffect(() => {
        const fetchBorder = async () => {
            bordersNameCopy = [];
            for (const key in borders) {
                const borderNameResponse = await axios.get('https://restcountries.eu/rest/v2/alpha/' + borders[key].border);
                await bordersNameCopy.push({borderName: borderNameResponse.data.name});
                await setBordersName(bordersNameCopy);
            }
            if (bordersNameCopy[0] !== undefined) {
                namesCopy = [];
                bordersNameCopy.map(name => {
                    namesCopy.push({borderName: name.borderName});
                });
                setNames(namesCopy);
            };
        };
        fetchBorder();
    }, [borders]);


    let namesList = namesCopy.map(name => {
        if (namesCopy.length > 0) {
            return(
                <li key={name.borderName}>{name.borderName}</li>
            );
        }
    });

    if (country[0] !== undefined) {
        return (
            <>
                <h1 className="heading">{props.name}</h1>
                <div className="content">
                    <div className="txtInformation">
                        <p><b>Capital:</b> {country[0].capital}</p>
                        <p><b>Population:</b> {country[0].population}</p>
                    </div>
                    <img src={country[0].flag} alt="flag" className="flag"/>
                </div>
                <div className="borders">
                    <h4>Borders with:</h4>
                    <ul>
                        {namesList}
                    </ul>
                </div>
            </>
        );
    }  else {
        return (
            <p style={{'textAlign': 'center'}}>Choose country!</p>
        )
    };
};

export default Info;