import React from 'react';
import './countries.css'

const Countries = props => {
    return (
        <p className="country" onClick={props.clicked}>{props.name}</p>
    );
};

export default Countries;